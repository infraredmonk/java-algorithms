# Java Algorithms

A collection of algorithms in Java. Following are the broad categories into which these are organized:

1. Amortized Analysis
2. Divide-and-Conquer
3. Dynamic Programming
4. Greedy
5. Stable Matching

Each Java file has a class with the suffix *Test in it which contains the main method to run the program. All the
programs added so far do not need any additional parameters to run.
