package com.infraredmonk.algorithms.amortizedanalysis;

import java.util.LinkedList;
import java.util.List;

final class SplayTreeNode {
    int value;
    SplayTreeNode right, left, parent;

    public SplayTreeNode(int value) {
        this.value = value;
        this.right = this.left = this.parent = null;
    }
}


public final class SplayTree {

    private SplayTreeNode rootNode;

    public void insert(int value) {
        if (rootNode == null) {
            rootNode = new SplayTreeNode(value);
        } else {
            SplayTreeNode insertedNode = insert(rootNode, value);
            splay(insertedNode);
        }
    }

    private SplayTreeNode insert(SplayTreeNode trav, int value) {
        SplayTreeNode node = new SplayTreeNode(value);
        while (node.parent == null) {
            if (trav.value < value) {
                if (trav.right == null) {
                    trav.right = node;
                    node.parent = trav;
                } else {
                    trav = trav.right;
                }
            } else {
                if (trav.left == null) {
                    trav.left = node;
                    node.parent = trav;
                } else {
                    trav = trav.left;
                }
            }
        }
        return node;
    }

    public void search(int value) {
        if (rootNode == null) {
            throw new RuntimeException("Splay tree does not exist. Cannot perform search operation.");
        } else {
            SplayTreeNode searchedNode = search(rootNode, value);
            splay(searchedNode);
        }
    }

    private SplayTreeNode search(SplayTreeNode trav, int value) {
        SplayTreeNode parentNode = trav.parent;
        while (trav != null && trav.value != value) {
            if (trav.value < value) {
                parentNode = trav;
                trav = trav.right;
            } else {
                parentNode = trav;
                trav = trav.left;
            }
        }
        if (trav != null) {
            return trav;
        } else {
            return parentNode;
        }
    }

    // Do this when node is to the right of the parent
    private void singleRotateLeft(SplayTreeNode node, SplayTreeNode parent) {
        if (parent.parent != null) {
            if (parent == parent.parent.right) {
                parent.parent.right = node;
            } else {
                parent.parent.left = node;
            }
        }
        if (node.left != null) {
            node.left.parent = parent;
        }
        node.parent = parent.parent;
        parent.parent = node;
        parent.right = node.left;
        node.left = parent;
    }

    // Do this when node is to the left of the parent
    private void singleRotateRight(SplayTreeNode node, SplayTreeNode parent) {
        if (parent.parent != null) {
            if (parent == parent.parent.left) {
                parent.parent.left = node;
            } else {
                parent.parent.right = node;
            }
        }
        if (node.right != null) {
            node.right.parent = parent;
        }
        node.parent = parent.parent;
        parent.parent = node;
        parent.left = node.right;
        node.right = parent;
    }

    // Do this when parent == grandParent.right and node == parent.left
    private void zigzagRotateLeft(SplayTreeNode node, SplayTreeNode parent, SplayTreeNode grandParent) {
        singleRotateRight(node, parent);
        singleRotateLeft(node, grandParent); // after right rotation grandParent is the parent of node
    }

    // Do this when parent == grandParent.left and node == parent.right
    private void zigzagRotateRight(SplayTreeNode node, SplayTreeNode parent, SplayTreeNode grandParent) {
        singleRotateLeft(node, parent);
        singleRotateRight(node, grandParent);
    }

    // Do this when parent == grandParent.right and node == parent.right
    private void rollerCoasterRotateLeft(SplayTreeNode node, SplayTreeNode parent, SplayTreeNode grandParent) {
        singleRotateLeft(parent, grandParent);
        singleRotateLeft(node, parent);
    }

    // Do this when parent == grandParent.left and node == parent.left
    private void rollerCoasterRotateRight(SplayTreeNode node, SplayTreeNode parent, SplayTreeNode grandParent) {
        singleRotateRight(parent, grandParent);
        singleRotateRight(node, parent);
    }

    public void splay(SplayTreeNode node) {
        while (node.parent != null) {
            SplayTreeNode parent = node.parent;
            SplayTreeNode grandParent = parent.parent;
            if (grandParent != null) {
                if (node == parent.left) {
                    if (parent == grandParent.left) {
                        rollerCoasterRotateRight(node, parent, grandParent);
                    } else if (parent == grandParent.right) {
                        zigzagRotateLeft(node, parent, grandParent);
                    }
                } else if (node == parent.right) {
                    if (parent == grandParent.left) {
                        zigzagRotateRight(node, parent, grandParent);
                    } else if (parent == grandParent.right) {
                        rollerCoasterRotateLeft(node, parent, grandParent);
                    }
                }
            } else {
                if (node == parent.left) {
                    singleRotateRight(node, parent);
                } else if (node == parent.right) {
                    singleRotateLeft(node, parent);
                }
            }
        }
        rootNode = node;
    }

    private List<Integer> preorderList;

    public void printTree() {
        preorderList = new LinkedList<>();
        preorder(rootNode);
        System.out.println();
        preorderList.forEach(i -> System.out.print(i + ", "));
        System.out.println();
    }

    private void preorder(SplayTreeNode trav) {
        preorderList.add(trav.value);
        if (trav.left != null) {
            preorder(trav.left);
        }
        if (trav.right != null) {
            preorder(trav.right);
        }
    }
}


final class SplayTreeTest {
    public static void main(String[] args) {
        SplayTree splayTree = new SplayTree();
        splayTree.insert(15);
        splayTree.insert(35);
        splayTree.insert(55);
        splayTree.insert(75);
        splayTree.insert(20);
        splayTree.insert(60);
        splayTree.insert(50);
        splayTree.printTree();
        splayTree.search(50);
        splayTree.printTree();
        splayTree.search(20);
        splayTree.printTree();
        splayTree.search(75);
        splayTree.printTree();
    }
}
