package com.infraredmonk.algorithms.amortizedanalysis;

public final class BinaryCounter {

    char[] binaryString;
    int flips;

    public BinaryCounter(char[] binaryString) {
        this.binaryString = binaryString;
    }

    public void increment() {
        flips = 0;
        int j = binaryString.length - 1;
        while (binaryString[j] == '1') {
            binaryString[j--] = '0';
            flips++;
        }
        binaryString[j] = '1';
        flips++;
    }

    public void printBinaryString() {
        System.out.println("Binary String: " + String.valueOf(binaryString) + "; flips: " + flips);
    }
}


final class BinaryCounterTest {
    public static void main(String[] args) {
        BinaryCounter binaryCounter = new BinaryCounter("00000".toCharArray());
        binaryCounter.printBinaryString();
        binaryCounter.increment();
        while (!String.valueOf(binaryCounter.binaryString).equals("11111")) {
            binaryCounter.increment();
            binaryCounter.printBinaryString();
        }
    }
}
