package com.infraredmonk.algorithms.dynamicprogramming;

import java.util.Arrays;
import java.util.Comparator;

final class WeightedJob {
    Integer startTime;
    Integer finishTime;
    Integer weight;

    public WeightedJob(int startTime, int finishTime, int weight) {
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.weight = weight;
    }
}


public final class WeightedIntervalScheduling {

    private final WeightedJob[] weightedJobs;
    private int[] jobCompatibilityArray;
    private int[] maxProfit;

    public WeightedIntervalScheduling(WeightedJob[] weightedJobs) {
        Arrays.sort(weightedJobs, Comparator.comparingInt(j -> j.finishTime));
        this.weightedJobs = weightedJobs;
    }

    public void maxProfitSchedule() {
        jobCompatibilityArray = new int[weightedJobs.length];
        maxProfit = new int[weightedJobs.length];
        for (int i = 0; i < weightedJobs.length - 1; i++) {
            int index = getLastMutuallyCompatibleJobIndex(i);
            jobCompatibilityArray[i] = index;
            if (index == -1) {
                maxProfit[i] = weightedJobs[i].weight;
            } else {
                maxProfit[i] = weightedJobs[i].weight + maxProfit[index];
            }
        }
    }

    private int getLastMutuallyCompatibleJobIndex(int i) {
        if (i != 0) {
            // the linear search can be further optimized by using binary search instead
            for (int j = i - 1; j >= 0; j--) {
                if (weightedJobs[i].startTime >= weightedJobs[j].finishTime) {
                    return j;
                }
            }
        }
        return -1; // There are no jobs which are mutually compatible with the input job in the array
    }

    public void printSchedule() {
        for (WeightedJob job : weightedJobs) {
            for (int i = 0; i < job.startTime; i++) {
                System.out.print(" ");
            }
            System.out.print(job.startTime);
            for (int i = 0; i < job.finishTime; i++) {
                if (i == (job.finishTime / 2)) {
                    System.out.print(job.weight);
                } else {
                    System.out.print("-");
                }
            }
            System.out.print(job.finishTime);
            System.out.println();
        }
    }

    public void printResults() {
        int maxValue = 0;
        int maxValueIdx = -1;
        for (int i = 0; i < maxProfit.length - 1; i++) {
            if (maxProfit[i] > maxValue) {
                maxValue = maxProfit[i];
                maxValueIdx = i;
            }
        }
        System.out.println("Results::");
        System.out.println("max value: " + maxValue + ", max value index: " + maxValueIdx);
        for (int i = maxValueIdx; i != -1; i = jobCompatibilityArray[i]) {
            System.out.print("job: (" +
                    weightedJobs[i].startTime + ", " +
                    weightedJobs[i].finishTime + ", " +
                    weightedJobs[i].weight + ")");
            System.out.println();
        }
    }
}


final class WeightedIntervalSchedulingTest {
    public static void main(String[] args) {
        WeightedJob[] jobs = new WeightedJob[] {
                new WeightedJob(0, 3, 3),
                new WeightedJob(0, 5, 4),
                new WeightedJob(1, 4, 2),
                new WeightedJob(3, 6, 1),
                new WeightedJob(3, 9, 5),
                new WeightedJob(4, 7, 2),
                new WeightedJob(5, 10, 2),
                new WeightedJob(8, 10, 1)
        };
        WeightedIntervalScheduling weightedIntervalScheduling = new WeightedIntervalScheduling(jobs);
        System.out.println("----------------");
        System.out.println("Input");
        System.out.println("----------------");
        weightedIntervalScheduling.printSchedule();

        System.out.println("----------------");
        System.out.println("Output");
        System.out.println("----------------");
        weightedIntervalScheduling.maxProfitSchedule();
        weightedIntervalScheduling.printSchedule();
        weightedIntervalScheduling.printResults();
    }
}
