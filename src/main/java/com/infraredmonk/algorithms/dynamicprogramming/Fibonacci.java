package com.infraredmonk.algorithms.dynamicprogramming;

public final class Fibonacci {

    public long recursive_fib(int n) {
        long result;
        if (n == 1 || n == 2) {
            result = 1;
        } else {
            result = recursive_fib(n - 1) + recursive_fib(n - 2);
        }
        return result;
    }

    private long[] store;

    public long memoized_recursive_fib(int n) {
        if (store == null) {
            store = new long[n + 1];
        }
        if (store[n] != 0) {
            return store[n];
        }
        long result;
        if (n == 1 || n == 2) {
            result = 1;
        } else {
            result = memoized_recursive_fib(n - 1) + memoized_recursive_fib(n - 2);
        }
        store[n] = result;
        return result;
    }

    public long space_optimized_fib(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        long a = 1, b = 1, result = 0;
        for (int i = 3; i <= n; i++) {
            result = a + b;
            a = b;
            b = result;
        }
        return result;
    }
}
