package com.infraredmonk.algorithms.dynamicprogramming;

import java.util.HashSet;
import java.util.Set;

final class GraphNode {
    private String label;

    public GraphNode(String label) {
        this.label = label;
    }
}


final class GraphEdge {
    private GraphNode source;
    private GraphNode destination;
    private Integer weight;

    public GraphEdge(GraphNode source, GraphNode destination, Integer weight) {
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }
}


public final class BellmanFord {


}


final class BellmanFordTest {
    public static void main(String[] args) {
        GraphNode[] vertices = new GraphNode[] {
                new GraphNode("A"),
                new GraphNode("B"),
                new GraphNode("C"),
                new GraphNode("D"),
                new GraphNode("E")
        };
        Set<GraphEdge> edges = new HashSet<GraphEdge>() {{
            add(new GraphEdge(vertices[0], vertices[1], 3));
            add(new GraphEdge(vertices[1], vertices[2], 3));
            add(new GraphEdge(vertices[2], vertices[3], 3));
            add(new GraphEdge(vertices[0], vertices[3], 3));
            add(new GraphEdge(vertices[2], vertices[4], 3));
            add(new GraphEdge(vertices[3], vertices[4], 3));
        }};

    }
}
