package com.infraredmonk.algorithms.stablematching;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public final class HospitalResident {

    /**
     * student: sunny will be unmatched hospital: mercy will not fill all it's positions
     */
    private final Map<String, LinkedList<String>> hospitalPreferences = new HashMap<String, LinkedList<String>>() {{
        put("Mercy", new LinkedList<>(Arrays.asList("Darrius", "Joseph")));
        put("City", new LinkedList<>(Arrays.asList("Darrius", "Arthur", "Sunny", "Latha", "Joseph")));
        put("General", new LinkedList<>(Arrays.asList("Darrius", "Arthur", "Joseph", "Latha")));
    }};
    private final Map<String, LinkedList<String>> studentPreferences = new HashMap<String, LinkedList<String>>() {{
        put("Arthur", new LinkedList<>(Arrays.asList("City")));
        put("Sunny", new LinkedList<>(Arrays.asList("City", "Mercy")));
        put("Joseph", new LinkedList<>(Arrays.asList("City", "General", "Mercy")));
        put("Latha", new LinkedList<>(Arrays.asList("Mercy", "City", "General")));
        put("Darrius", new LinkedList<>(Arrays.asList("City", "Mercy", "General")));
    }};
    private final Map<String, String[]> hospitalSlots = new HashMap<String, String[]>() {{
        put("Mercy", new String[2]);
        put("City", new String[2]);
        put("General", new String[2]);
    }};
    private final Queue<String> students = new LinkedList<>();

    public HospitalResident() {
        students.addAll(studentPreferences.keySet());
    }

    public void performMatch() {
        while (!students.isEmpty()) {
            String student = students.remove();
            if (!studentPreferences.get(student).isEmpty()) {
                String hospital = studentPreferences.get(student).remove();
                System.out.println(student + "'s preferred hospital is " + hospital);
                List<String> preferredStudents = hospitalPreferences.get(hospital);
                if (preferredStudents.contains(student)) { // if student is preferred by the hospital
                    int rankOfStudent = preferredStudents.indexOf(student);
                    String[] slots = hospitalSlots.get(hospital);
                    for (int i = 0; i < slots.length; i++) {
                        if (slots[i] != null) { // slot is not empty
                            int rankOfStudentInSlot = hospitalPreferences.get(hospital).indexOf(slots[i]);
                            if (rankOfStudent < rankOfStudentInSlot) {
                                String current = student;
                                for (int j = i; j < slots.length; j++) {
                                    String prev = slots[j];
                                    hospitalSlots.get(hospital)[j] = current;
                                }
                                break;
                            } else {
                                continue;
                            }
                        } else { // slot is empty
                            hospitalSlots.get(hospital)[i] = student;
                            break;
                        }
                    }
                }
            } else {
                System.out.println(student + " does not have any preferences left!");
            }
        }
    }

    public void printPreferences() {
        System.out.println("--------------");
        hospitalSlots.forEach((hospital, slots) -> {
            System.out.println("Hospital: " + hospital);
            for (String slot : slots) {
                System.out.println("\t" + slot);
            }
        });
    }

    public static void main(String[] args) {
        HospitalResident hospitalResident = new HospitalResident();
        hospitalResident.performMatch();
        hospitalResident.printPreferences();
    }
}
