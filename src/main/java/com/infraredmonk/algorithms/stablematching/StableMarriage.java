package com.infraredmonk.algorithms.stablematching;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public final class StableMarriage {

    public Map<String, LinkedList<String>> menPreferences;
    public Map<String, LinkedList<String>> womenPreferences;
    public Queue<String> freeMen = new LinkedList<>();
    public Map<String, String> prefMatches = new HashMap<>();

    public StableMarriage(
            Map<String, LinkedList<String>> menPreferences,
            Map<String, LinkedList<String>> womenPreferences) {
        this.menPreferences = menPreferences;
        this.womenPreferences = womenPreferences;
        freeMen.addAll(menPreferences.keySet());
    }

    public void findMatches() {
        while (!freeMen.isEmpty()) {
            String newMan = freeMen.remove();
            String woman = menPreferences.get(newMan).remove();
            System.out.println(newMan + " proposes " + woman);
            if (prefMatches.containsKey(woman)) {
                String curMan = prefMatches.get(woman);
                int curManRank = womenPreferences.get(woman).indexOf(curMan);
                int newManRank = womenPreferences.get(woman).indexOf(newMan);
                if (curManRank > newManRank) { // if rank is larger, preference is lower
                    System.out.println(woman + " prefers new partner " + newMan + " over current partner " + curMan);
                    freeMen.add(curMan);
                    prefMatches.put(woman, newMan);
                } else {
                    System.out.println(woman + " prefers current partner " + curMan + " over new partner " + newMan);
                    freeMen.add(newMan);
                }
            } else {
                System.out.println(woman + " has tentatively accepted " + newMan + "'s proposal");
                prefMatches.put(woman, newMan);
            }
        }
    }

    public void printMatches() {
        System.out.println("--------------");
        prefMatches.forEach((man, woman) -> System.out.println(man + " has been matched with " + woman));
    }
}
