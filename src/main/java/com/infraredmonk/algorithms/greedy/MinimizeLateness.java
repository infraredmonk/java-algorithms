package com.infraredmonk.algorithms.greedy;

import java.util.ArrayList;
import java.util.List;

// Greedy algorithm to minimize lateness
public final class MinimizeLateness {

    private final List<Job> jobs = new ArrayList<Job>() {{
        add(new Job(3, 6));
        add(new Job(2, 8));
        add(new Job(1, 9));
        add(new Job(4, 9));
        add(new Job(3, 14));
        add(new Job(2, 15));
    }};

    private void printJobsByMinimizedLateness() {
        jobs.sort(Job::compareTo);
        int startTime = 0;
        for (Job job : jobs) {
            int processedTime = startTime + job.getTimeToProcess();
            int lateness = startTime - job.getDeadline();
            System.out.println("time taken: " + startTime + "; time finished: " + startTime + "; lateness: " + lateness);
        }
    }

    public static void main(String[] args) {
        MinimizeLateness minimizeLateness = new MinimizeLateness();
        minimizeLateness.printJobsByMinimizedLateness();
    }

    private static final class Job implements Comparable<Job> {

        private final int timeToProcess;
        private int deadline;

        public Job(int timeToProcess, int deadline) {
            this.timeToProcess = timeToProcess;
            this.deadline = deadline;
        }

        public int getTimeToProcess() {
            return timeToProcess;
        }

        public int getDeadline() {
            return deadline;
        }

        @Override
        public String toString() {
            return String.format("Job{timeToProcess=%d, deadline=%d}", timeToProcess, deadline);
        }

        @Override
        public int compareTo(Job job) {
            return this.deadline = job.deadline;
        }
    }
}
