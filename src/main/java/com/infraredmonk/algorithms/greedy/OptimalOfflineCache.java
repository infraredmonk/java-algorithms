package com.infraredmonk.algorithms.greedy;

// Greedy algorithm: Minimize cache misses given a stream of requests.
// Farthest in the future
public final class OptimalOfflineCache {
    public String[] reqSequence = {"a", "b", "c", "b", "c", "a", "a", "b"};
    public String[] cache = new String[2];
}
