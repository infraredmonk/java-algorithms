package com.infraredmonk.algorithms.greedy;

import java.util.ArrayList;
import java.util.List;

final class Job implements Comparable<Job> {

    private final int startTime;
    private final int endTime;

    public Job(int startTime, int endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    @Override
    public int compareTo(Job job) {
        return (this.endTime - job.endTime);
    }

    @Override
    public String toString() {
        return String.format("Job{startTime=%d, endTime=%d}", startTime, endTime);
    }
}


public final class IntervalScheduling {

    private static final List<Job> jobs = new ArrayList<Job>() {{
        add(new Job(1, 10));
        add(new Job(2, 10));
        add(new Job(1, 3));
        add(new Job(4, 6));
        add(new Job(4, 6));
        add(new Job(7, 9));
        add(new Job(7, 10));
        add(new Job(4, 8));
    }};

    public List<Job> getMaxMutuallyCompatibleJobs() {
        jobs.sort(Job::compareTo);
        List<Job> maxMutuallyCompatibleJobs = new ArrayList<>();
        int prevEndTime = 0;
        for (Job job : jobs) {
            if (job.getStartTime() > prevEndTime) {
                maxMutuallyCompatibleJobs.add(job);
                prevEndTime = job.getEndTime();
            }
        }
        return maxMutuallyCompatibleJobs;
    }
}


final class IntervalSchedulingTest {
    public static void main(String[] args) {
        IntervalScheduling intervalScheduling = new IntervalScheduling();
        List<Job> maxMutuallyCompatibleJobs = intervalScheduling.getMaxMutuallyCompatibleJobs();
        maxMutuallyCompatibleJobs.forEach(System.out::println);
    }
}
