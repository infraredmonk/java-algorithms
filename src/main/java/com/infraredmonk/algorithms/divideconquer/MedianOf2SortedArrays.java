package com.infraredmonk.algorithms.divideconquer;

/**
 * Find the median from a sequence of numbers which exist as part of 2 sorted arrays.
 * <p>
 * Assumptions: 1. Arrays cannot be empty. 2. No two values across both arrays are same.
 */
public final class MedianOf2SortedArrays {
    int[] p = {1, 12, 15, 26, 38};
    int[] q = {2, 13, 17, 30, 45};

    public int getMedian(int[] arr1, int[] arr2) {
        if (arr1.length == 2 && arr2.length == 2) {
            return (Integer.max(arr1[0], arr2[0]) + Integer.min(arr1[1], arr2[1]))/2;
        } else {
            int arr1_median = arr1[(arr1.length - 1)/2];
        }
        return -1;
    }

    public static void main(String[] args) {
    }
}
