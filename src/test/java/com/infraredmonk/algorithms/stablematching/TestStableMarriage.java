package com.infraredmonk.algorithms.stablematching;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

public class TestStableMarriage {

    @Test
    public void testStableMarriage_defaultCase() {
        HashMap<String, LinkedList<String>> menPreferenceMap = new HashMap<String, LinkedList<String>>() {{
            put("Steve", new LinkedList<>(Arrays.asList("Emma", "Harper", "Ava", "Olivia")));
            put("Will", new LinkedList<>(Arrays.asList("Ava", "Harper", "Emma", "Olivia")));
            put("Anthony", new LinkedList<>(Arrays.asList("Ava", "Olivia", "Emma", "Harper")));
            put("Donald", new LinkedList<>(Arrays.asList("Harper", "Ava", "Olivia", "Emma")));
        }};
        HashMap<String, LinkedList<String>> womenPreferenceMap = new HashMap<String, LinkedList<String>>() {{
            put("Olivia", new LinkedList<>(Arrays.asList("Will", "Donald", "Steve", "Anthony")));
            put("Ava", new LinkedList<>(Arrays.asList("Steve", "Donald", "Anthony", "Will")));
            put("Harper", new LinkedList<>(Arrays.asList("Anthony", "Will", "Donald", "Steve")));
            put("Emma", new LinkedList<>(Arrays.asList("Anthony", "Steve", "Will", "Donald")));
        }};
        StableMarriage stableMarriage = new StableMarriage(menPreferenceMap, womenPreferenceMap);
        stableMarriage.findMatches();
        stableMarriage.printMatches();
    }
}
