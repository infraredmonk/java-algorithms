package com.infraredmonk.algorithms.dynamicprogramming;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestFibonacci {

    private final Fibonacci fibonacci = new Fibonacci();

    @Test
    public void testFibonacci_recursiveWithMemoizationCase() {
        long fib = fibonacci.memoized_recursive_fib(195);
        assertEquals(fib, 9150458355941216770L);
    }

    @Test
    public void testFibonacci_recursiveCase() {
        long fib = fibonacci.recursive_fib(42);
        assertEquals(fib, 267914296);
    }

    @Test
    public void testFibonacci_spaceOptimizedCase() {
        long fib = fibonacci.space_optimized_fib(195);
        assertEquals(fib, 9150458355941216770L);
    }
}
